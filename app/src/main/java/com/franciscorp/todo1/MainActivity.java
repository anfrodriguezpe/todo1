package com.franciscorp.todo1;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.franciscorp.todo1.dtoResponse.WeatherResponse;
import com.franciscorp.todo1.interfaces.WeatherResponseInterface;
import com.franciscorp.todo1.managers.ManagerGetWeather;
import com.franciscorp.todo1.managers.ManagerLocation;
import com.franciscorp.todo1.util.PermissionUtils;

import static com.franciscorp.todo1.managers.ManagerLocation.LOCATION_PERMISSION_REQUEST_CODE;
import static com.franciscorp.todo1.util.PermissionUtils.RationaleDialog.TAG_DIALOG;

public class MainActivity extends AppCompatActivity implements ManagerLocation.UpdateLocation, WeatherResponseInterface {

    private ManagerLocation managerLocation;
    private ManagerGetWeather managerGetWeather;
    private Location location;

    private TextView timezone, temperature, summary, humidity, precipProbability;
    private ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timezone = findViewById(R.id.timezone);
        temperature = findViewById(R.id.temperature);
        summary = findViewById(R.id.summary);
        humidity = findViewById(R.id.humidity);
        precipProbability = findViewById(R.id.precip_probability);
        icon = findViewById(R.id.icon);

        managerGetWeather = new ManagerGetWeather(this);
        managerLocation = new ManagerLocation(this);
        location = managerLocation.getmLastLocation();
        getWeather();
    }

    private void getWeather() {
        if (location!=null){
            managerGetWeather.getWeather(location);
        }
    }

    public void btnRefresh(View view) {
        location = managerLocation.getmLastLocation();
        getWeather();
    }

    @Override
    public void updateLocation(Location location) {
        this.location = location;
        getWeather();
    }

    /**
     * start service locationManager
     */
    @Override
    public void onStart() {
        super.onStart();
        managerLocation.connect();
    }

    /**
     * finish service locationManager
     */
    @Override
    public void onStop() {
        super.onStop();
        managerLocation.disconnect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getWeather();
                } else {
                    PermissionUtils.RationaleDialog rationaleDialog = PermissionUtils.RationaleDialog.newInstance(LOCATION_PERMISSION_REQUEST_CODE, true, android.Manifest.permission.ACCESS_FINE_LOCATION);
                    if (!rationaleDialog.isAdded()) {
                        rationaleDialog.show(getSupportFragmentManager(), TAG_DIALOG);
                    }

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void showWeather(WeatherResponse weatherResponse) {
        timezone.setText(weatherResponse.getTimeZone());
        temperature.setText(weatherResponse.getCurrently().getTemperature()+"º");
        summary.setText(weatherResponse.getCurrently().getSummary());
        humidity.setText(weatherResponse.getCurrently().getHumidity()*100+"%");
        precipProbability.setText(weatherResponse.getCurrently().getPrecipProbability()*100+"%");
        changeImage(weatherResponse.getCurrently().getIcon());
    }

    //possibles values: clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night.
    private void changeImage(String iconName) {
        iconName = iconName.replaceAll("-", "_");
        int imageResource = getResources().getIdentifier(iconName, "drawable", getPackageName());
        if (imageResource!=0) {
            icon.setImageResource(imageResource);
        }else {
            icon.setImageResource(R.mipmap.ic_launcher);
        }
    }
}
