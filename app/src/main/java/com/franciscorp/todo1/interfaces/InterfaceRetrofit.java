package com.franciscorp.todo1.interfaces;

import com.franciscorp.todo1.dtoResponse.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface InterfaceRetrofit {

    /**
     * get Weather
     *
     * @param url
     * @return
     */
    @GET
    Call<WeatherResponse> getWeather(@Url String url);
}
