package com.franciscorp.todo1.interfaces;

import com.franciscorp.todo1.dtoResponse.WeatherResponse;

public interface WeatherResponseInterface {

    /**
     * send response service
     *
     * @param weatherResponse
     */
    void showWeather(WeatherResponse weatherResponse);


}
