package com.franciscorp.todo1.managers;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.franciscorp.todo1.dtoResponse.WeatherResponse;
import com.franciscorp.todo1.interfaces.InterfaceRetrofit;
import com.franciscorp.todo1.interfaces.WeatherResponseInterface;
import com.franciscorp.todo1.util.CheckConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManagerGetWeather {

    private String url = "https://api.darksky.net/";
    private Context context;
    private ManagerProgressDialog progressDialog;
    public static final String TAG = "serviceWeather";

    public ManagerGetWeather(Context context){
        this.context = context;
        progressDialog = new ManagerProgressDialog(context);
    }

    public void getWeather(Location location) {
        if (CheckConnection.isConnected(context)) {

            progressDialog.showProgress();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            InterfaceRetrofit retrofitIR = retrofit.create(InterfaceRetrofit.class);

            String url1 = "https://api.darksky.net/forecast/7d58fc3d3167745c0a5f0851b8ee7e49/" + location.getLatitude() + "," + location.getLongitude();

            Call<WeatherResponse> call = retrofitIR.getWeather(url1);

            call.enqueue(new Callback<WeatherResponse>() {
                @Override
                public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {

                    if (response.body() != null) {

                        WeatherResponse weatherResponse = response.body();
                        try {
                            ((WeatherResponseInterface) context).showWeather(weatherResponse);
                        } catch (ClassCastException e) {
                            Log.e(TAG, context.toString() + " must implement OnHeadlineSelectedListener");
                        }

                        Log.i(TAG, weatherResponse.getTimeZone());
                    }
                    progressDialog.dismissProgress();
                }

                @Override
                public void onFailure(Call<WeatherResponse> call, Throwable t) {
                    progressDialog.dismissProgress();
                    if (t != null) {
                        if (t.getLocalizedMessage() != null) {
                            Log.e(TAG, t.getLocalizedMessage());
                        }
                    }
                }
            });
        }else{
            CheckConnection.check(context);
        }
    }
}
