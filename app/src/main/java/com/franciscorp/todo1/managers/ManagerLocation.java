package com.franciscorp.todo1.managers;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.franciscorp.todo1.R;
import com.franciscorp.todo1.util.PermissionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


public class ManagerLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Context context;
    private LocationManager locationManager;
    private UpdateLocation mCallback;
    private boolean showTurnOnGPS = true;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 123;

    public ManagerLocation(Context context) {

        this.context =context;
        // Create an instance of GoogleAPIClient.

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        checkGPS();

        try {
            mCallback = (UpdateLocation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        mCallback.updateLocation(location);
        removeListener();
        disconnect();

    }

    public interface UpdateLocation {
        /** Called by HeadlinesFragment when a list item is selected */
        void updateLocation(Location location);
    }

    public boolean checkGPS() {
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            turnGPSOn();
        }
        return isGPSEnabled;
    }

    public static boolean checkGPS(Context context) {
        LocationManager locationManager;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            turnGPSOn(context);
        }
        return isGPSEnabled;
    }

    public static synchronized void turnGPSOn(final Context mContext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.ThemeDialogCustom);
        builder.setMessage(mContext.getString(R.string.gps_not_enable))
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.btn_accept), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                }).setNegativeButton(mContext.getString(R.string.btn_cancel), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).create().show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (isAllowed()) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                mCallback.updateLocation(mLastLocation);
                System.out.print("location " + mLastLocation.toString());
            }
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(300*1000);//milliseconds
            mLocationRequest.setFastestInterval(200*1000);//milliseconds
            mLocationRequest.setSmallestDisplacement(500);//meter
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ManagerLocation.this);
        }
    }

    public synchronized void turnGPSOn() {

        if (showTurnOnGPS) {
            showTurnOnGPS = false;
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.ThemeDialogCustom);
            builder.setMessage(context.getString(R.string.gps_not_enable))
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.btn_accept), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            showTurnOnGPS = true;
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(intent);
                        }
                    }).setNegativeButton(context.getString(R.string.btn_cancel), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    showTurnOnGPS = true;
                    Toast.makeText(context,
                            R.string.gps_required_toast,
                            Toast.LENGTH_SHORT)
                            .show();

                }
            }).create().show();
        }
    }

    private synchronized Boolean isAllowed() {

        try {
            boolean permissionfineLocation= (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            boolean permissionCoarseLocation= (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
            if (permissionfineLocation || permissionCoarseLocation) {
                return true;
            }else{
                if (context instanceof AppCompatActivity) {
                    PermissionUtils.requestPermission((AppCompatActivity) context, LOCATION_PERMISSION_REQUEST_CODE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION, true);
                } else if (context instanceof FragmentActivity) {
                    PermissionUtils.requestPermission((FragmentActivity) context, LOCATION_PERMISSION_REQUEST_CODE,
                            android.Manifest.permission.ACCESS_FINE_LOCATION, true);
                }

                return false;
            }
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void connect() {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public void disconnect() {
        mGoogleApiClient.disconnect();
    }

    public Location getmLastLocation() {

        if (isAllowed()) {
            checkGPS();
            if (mGoogleApiClient.isConnected()) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (mLastLocation != null) {
                    System.out.print("location " + mLastLocation.toString());
                }
            }else{
                mGoogleApiClient.connect();
            }
        }
        return mLastLocation;
    }

    public void setmLastLocation(Location mLastLocation) {
        this.mLastLocation = mLastLocation;
    }

    /**
     * Metodo utilizado para remover el listener del GPS
     */
    public void removeListener(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }
}
