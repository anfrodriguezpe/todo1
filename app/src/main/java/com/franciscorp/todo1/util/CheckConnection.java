package com.franciscorp.todo1.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.franciscorp.todo1.R;


public class CheckConnection {

    private Context mContext;
    private static String TAG = "NetworkInfo";
    private static boolean isShowingDialog = false;

    public CheckConnection(Context context){
        mContext = context;
    }

    public void check(){
        if(!isConnected()){

            String texto=mContext.getResources().getString(R.string.error_conection);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setTitle(R.string.app_name);
            alertDialogBuilder.setMessage(texto);
            alertDialogBuilder.setPositiveButton(mContext.getText(R.string.btn_accept),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();

            if (!isShowingDialog) {
                isShowingDialog = true;
                alertDialog.show();
            }

            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    isShowingDialog = false;
                }
            });

        }
    }
    public boolean isConnected() {

        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);

            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
            return false;
        }
        return status;
    }

    public static void check(Context context){
        if(!isConnected(context)){

            String texto=context.getResources().getString(R.string.error_conection);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(R.string.app_name);
            alertDialogBuilder.setMessage(texto);
            alertDialogBuilder.setPositiveButton(context.getText(R.string.btn_accept),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();

            if (!isShowingDialog) {
                isShowingDialog = true;
                alertDialog.show();
            }

            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    isShowingDialog = false;
                }
            });
        }
    }

    public static boolean isConnected(Context context){

        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);

            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
            return false;
        }
        return status;
    }
}