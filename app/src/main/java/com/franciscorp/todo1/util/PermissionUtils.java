package com.franciscorp.todo1.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.franciscorp.todo1.R;

import static com.franciscorp.todo1.managers.ManagerLocation.LOCATION_PERMISSION_REQUEST_CODE;
import static com.franciscorp.todo1.util.PermissionUtils.RationaleDialog.TAG_DIALOG;

public abstract class PermissionUtils {
    public static final String TAG = "ScreenshotService";

    public static void requestPermission(AppCompatActivity activity, int requestId, String permission, boolean finishActivity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog rationaleDialog = PermissionUtils.RationaleDialog.newInstance(LOCATION_PERMISSION_REQUEST_CODE, finishActivity, permission);
            if (!rationaleDialog.isAdded()) {
                rationaleDialog.show(activity.getSupportFragmentManager(), TAG_DIALOG);
            }
        } else {
            // permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestId);
        }
    }

    public static void requestPermission(FragmentActivity activity, int requestId,
                                         String permission, boolean finishActivity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog rationaleDialog = PermissionUtils.RationaleDialog.newInstance(LOCATION_PERMISSION_REQUEST_CODE, finishActivity, permission);
            if (!rationaleDialog.isAdded()) {
                rationaleDialog.show(activity.getSupportFragmentManager(), TAG_DIALOG);
            }
        } else {
            // permission has not been granted yet, request it.
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestId);

        }
    }

    public static boolean isPermissionGranted(String[] grantPermissions, int[] grantResults,
                                              String permission) {
        for (int i = 0; i < grantPermissions.length; i++) {
            if (permission.equals(grantPermissions[i])) {
                return grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
        }
        return false;
    }

    public static class PermissionDeniedDialog extends DialogFragment {

        private static final String ARGUMENT_FINISH_ACTIVITY = "finish";

        private boolean mFinishActivity = false;

        public static PermissionDeniedDialog newInstance(boolean finishActivity) {
            Bundle arguments = new Bundle();
            arguments.putBoolean(ARGUMENT_FINISH_ACTIVITY, finishActivity);

            PermissionDeniedDialog dialog = new PermissionDeniedDialog();
            dialog.setArguments(arguments);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            mFinishActivity = getArguments().getBoolean(ARGUMENT_FINISH_ACTIVITY);

            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.location_permission_denied)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (mFinishActivity) {
                try {
                    Toast.makeText(getActivity(),
                            R.string.permission_required_toast,
                            Toast.LENGTH_SHORT)
                            .show();
                    getActivity().finish();
                }catch (Exception e){
                    Log.e(TAG, "error finish activity "+ e.getLocalizedMessage());
                }
            }
        }
    }

    public static class RationaleDialog extends DialogFragment {

        public static final String TAG_DIALOG= "dialog";
        private static final String ARGUMENT_PERMISSION_REQUEST_CODE = "requestCode";
        private static final String ARGUMENT_FINISH_ACTIVITY = "finish";
        private static final String ARGUMENT_PERMISSION = "permission";

        private boolean mFinishActivity = false;
        private static RationaleDialog dialog = null;

        public static RationaleDialog newInstance(int requestCode, boolean finishActivity, String permission) {
            Bundle arguments = new Bundle();
            arguments.putInt(ARGUMENT_PERMISSION_REQUEST_CODE, requestCode);
            arguments.putBoolean(ARGUMENT_FINISH_ACTIVITY, finishActivity);
            arguments.putString(ARGUMENT_PERMISSION, permission);
            if (dialog==null) {
                dialog = new RationaleDialog();

            }
            dialog.setArguments(arguments);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle arguments = getArguments();
            final int requestCode = arguments.getInt(ARGUMENT_PERMISSION_REQUEST_CODE);
            final String permission = arguments.getString(ARGUMENT_PERMISSION);
            mFinishActivity = arguments.getBoolean(ARGUMENT_FINISH_ACTIVITY);

            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.permission_rationale_location)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // After click on Ok, request the permission.
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{permission},
                                    requestCode);
                            // Do not finish the Activity while requesting permission.
                            mFinishActivity = false;
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .create();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);

            if (mFinishActivity) {
                try {
                    Toast.makeText(getActivity(),
                            R.string.permission_required_toast,
                            Toast.LENGTH_SHORT)
                            .show();
                    getActivity().finish();
                }catch (Exception e){
                    Log.e(TAG, "error finish activity "+ e.getLocalizedMessage());
                }
            }
        }
    }
}
